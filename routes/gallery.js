const express = require('express')
const router = express.Router()
const multer = require('multer')
const Gallery = require('../models/Gallery')
const passport = require('passport')
const uploadImage = require('./helpers.js')



//Save file to server storage
const storage = multer.memoryStorage({
    destination: (req, file, cb) => {
        cb(null, 'https://storage.googleapis.com/travelogue_image_bucket_1/')
    },
    filename: (req, file, cb) => {
        let fileType = ''
        if(file.mimetype === 'image/gif') {
            fileType = 'gif'
        }
        if(file.mimetype === 'image/png') {

        }
        if(file.mimetype === 'image/jpeg') {
            fileType = 'jpg'
        }
        cb(null, 'image-'+Date.now() + '.' + fileType)
    }
})

const upload = multer({storage: storage})

//post data to local server. for this to work change mutler to diskStorage
router.post('/uploadImage1', upload.single('file'), async (req, res, next) => {
    if(!req.file) {
        return res.status(500).send({success: false, msg: 'Upload fail'})
    }else{
        req.body.imageUrl = 'http://localhost:8080/images/' + req.file.filename
        Gallery.create(req.body, (err, gallery) => {
            if(err){
                console.log(err)
                return next(err)
            }
            res.json(gallery)
        })
    }
})

//post data
router.post('/uploadImage', upload.single('file'), async (req, res, next) => {
    if(!req.file) {
        return res.status(500).send({success: false, msg: 'Upload fail'})
    }else{
        req.body.imageUrl = await uploadImage(req.file)
        Gallery.create(req.body, (err, gallery) => {
            if(err){
                console.log(err)
                return next(err)
            }
            res.json(gallery)
        })
    }
})

//get data by id
router.get('/images/:id', (req, res, next) => {
    Gallery.findById(req.params.id, (err, gallery) => {
        if(err) return next(err)
        res.json(gallery)
    })
})

//get data by id
router.get('/travelImages/:id', (req, res, next) => {
    Gallery.find({travelId: req.params.id}, (err, gallery) => {
        if(err) return next(err)
        res.json(gallery)
    }).sort({"uploaded" : -1})
})

module.exports = router