const util = require('util')
const path = require('path')
const { Storage } = require('@google-cloud/storage') 

const gc = new Storage({
    keyFilename: path.join(__dirname, "../travelogue-294811-1ef23c393966.json"),
    projectId: 'travelogue-294811'
})
const { format } = util

const bucket = gc.bucket('travelogue_image_bucket_1')
/**
 *
 * @param { File } object file object that will be uploaded
 * @description - This function does the following
 * - It uploads a file to the image bucket on Google Cloud
 * - It accepts an object as an argument with the
 *   "originalname" and "buffer" as keys
 */

const uploadImage = (file) => new Promise((resolve, reject) => {
  const { originalname, buffer } = file

  const blob = bucket.file(originalname.replace(/ /g, "_") + new Date().getMilliseconds())
  const blobStream = blob.createWriteStream({
    resumable: false
  })

  blobStream.on('finish', () => {
    const publicUrl = format(
      `https://storage.googleapis.com/${bucket.name}/${blob.name}`
    )
    resolve(publicUrl)
  })
  .on('error', () => {
    reject(`Unable to upload image, something went wrong`)
  })
  .end(buffer)

})

module.exports = uploadImage