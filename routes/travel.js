const express = require('express')
const router = express.Router()
const Travel = require('../models/Travel')
const config = require('../config/database')
const passport = require('passport')

//add travel
router.post('/add',passport.authenticate('jwt', {session:false}),  (req, res, next) => {
    let newTravel = new Travel({
        title: req.body.title,
        destination: req.body.destination,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        description: req.body.description,
        userId: req.body.userId
    })
    Travel.addTravel(newTravel, (err, travel) => {
        if(err) {
            res.json({success: false, msg:'travel not added. Error occurred'})
        } else {
            res.json({success: true, travel: travel})
        }
    })
})

//get travel by travel id
router.get('/:id', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    Travel.getTravelById(req.params.id, (err, travel) => {
        if(err) return next(err)
        res.json(travel)
    })
})

//get travel by user id
router.get('/user/:id', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    Travel.getTravelsByUserId(req.params.id, (err, travel) => {
        if (err) {
            console.log("ERROR " + err);
            return next(err)
        }
        res.json(travel)
    })
})

//Update Travel details
router.put('/:id', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let updatedTravel = {}
    if (req.body.title) {
        updatedTravel.title = req.body.title
    }
    if (req.body.destination) {
        updatedTravel.destination = req.body.destination
    }
    if (req.body.startDate) {
        updatedTravel.startDate = req.body.startDate
    }
    if (req.body.endDate) {
        updatedTravel.endDate = req.body.endDate
    }
    if (req.body.description) {
        updatedTravel.description = req.body.description
    }
    
    updatedTravel._id = req.params.id
    
    Travel.updateTravel(updatedTravel, (err, travel) => {
        if(err) {
            res.json({success: false, msg:'travel details not updated'})
        }else{
            res.json({success: true, msg:'travel details updated'})
        }
    })
})

module.exports = router