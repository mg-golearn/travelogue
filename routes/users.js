const express = require('express')
const router = express.Router()
const passport = require('passport')
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const config = require('../config/database')

//Register
router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        dob: req.body.dob,
        interests: req.body.interests,
        scribble: req.body.scribble
    })

    User.getUserByUserName(newUser.username, (err, user) => {
        if(err) {
            res.json({success: false, msg:'Error while checking for availability'})
        }else{
            if(!user){
                //put in db only if a user does not exist
                User.addUser(newUser, (err, user) => {
                    if(err) {
                        res.json({success: false, msg:'Registration failed'})
                    }else{
                        res.json({success: true, msg:'User registered'})
                    }
                })
            }else{
                res.json({success: false, msg:'User name already exists'});
            }
        }
    } )
    
})

//Authenticate
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username
    const password = req.body.password
    User.getUserByUserName(username, (err, user) => {
        if(err) throw err

        if(!user){
            return res.json({success: false, msg:'User not found'})
        }
        if(user.password){
            User.comparePassword(password, user.password, (err, isMatch) => {
                if(err) throw err
                if(isMatch){
                    const token = jwt.sign({user}, config.secret, {
                        expiresIn: 604800
                    })
    
                    res.json({
                        success: true,
                        token: 'JWT ' + token,
                        user: {
                            id: user._id,
                            name: user.name,
                            username: user.username,
                            email: user.email,
                            dob: user.dob,
                            interests: user.interests,
                            scribble: user.scribble
                        }
                    })
                }else{
                    return res.json({success: false, msg:'Wrong password'})
                }
            })
        }
    })
})

//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    res.json({user: req.user})
})

//check password
router.post('/checkPwd', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const password = req.body.password
    const id = req.body.id
    User.getUserById(id, (err, user) => {
        if(err) throw err
        if(!user) {
            return res.json({success: false, msg:'User not found'})
        } else {
            User.comparePassword(password, user.password, (err, isMatch) => {
                if(err) throw err
                if(isMatch){
                    res.json({
                        success: true,
                        msg: 'matched'
                    })
                }else{
                    return res.json({success: false, msg:'password mismatch'})
                }
            })
        }
    })
})

//Update Password
router.put('/updatePassword/:id', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let updatedUser = {}
    updatedUser.password = req.body.password
    updatedUser._id = req.params.id
    
    User.updatePassword(updatedUser, (err, user) => {
        if(err) {
            res.json({success: false, msg:'Password NOT updated'})
        }else{
            res.json({success: true, msg:'password Updated'})
        }
    })
})

//Update Scribble
router.put('/updateInfo/:id', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    const id = req.params.id
    let updatedUser = {}
    updatedUser.scribble = req.body.scribble

    User.findByIdAndUpdate(id, updatedUser, (err, user) => {
        if(err) {
            res.json({success: false, msg:'Info updated'})
        }else{
            res.json({success: true, msg:'Info not Updated'})
        }
    })
})

module.exports = router