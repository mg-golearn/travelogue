const mongoose = require('mongoose')

const GallerySchema = new mongoose.Schema({
    id: String,
    imageUrl: String,
    travelId: String,
    imageDesc: String,
    uploaded: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Gallery', GallerySchema)