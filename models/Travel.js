const mongoose = require('mongoose')
const config = require('../config/database')

//User Schema
const TravelSchema = mongoose.Schema({
    title: String,
    destination: String,
    startDate: Date,
    endDate: Date,
    description: String,
    userId: String
})

const Travel = module.exports = mongoose.model('Travel', TravelSchema)

//add travel details
module.exports.addTravel = function(newTravel, callback) {
    newTravel.save(callback)
}

//get travel details
module.exports.getTravelById = function(id, callback) {
    Travel.findById(id, callback)
}

//get all travels for a user
module.exports.getTravelsByUserId = function(_userId, callback) {
    Travel.find({ userId: _userId}, callback);
}

module.exports.updateTravel = function(updTravel, callback) {
    Travel.findByIdAndUpdate(updTravel._id, updTravel, callback)
}