const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const config = require('../config/database')

//User Schema
const UserSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    dob: {
        type: String,
        required: false
    },
    interests: {
        type: String,
        required: false
    },
    scribble: {
        type: String,
        required: false
    }
})

const User = module.exports = mongoose.model('User', UserSchema)

module.exports.getUserById = function(id, callback) {
    User.findById(id, callback)
}

module.exports.getUserByUserName = function(username, callback) {
    const query = {username: username}
    User.findOne(query, callback)
}

module.exports.addUser = function(newUser, callback){
    newUser.dob = new Date(newUser.dob).toDateString()
    bcrypt.genSalt(10, (err, salt) => {
        if(err) throw err
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash
            newUser.save(callback)
        })
    })
}

module.exports.updatePassword = function(updUser, callback){
    bcrypt.genSalt(10, (err, salt) => {
        if(err) throw err
        bcrypt.hash(updUser.password, salt, (err, hash) => {
            updUser.password = hash
            User.findByIdAndUpdate(updUser._id, updUser, callback)
        })
    })
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if (err) throw err
        callback(null, isMatch)
    })
}