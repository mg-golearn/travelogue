const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')
const mongoose = require('mongoose')
const users = require('./routes/users')
const config = require('./config/database')
const router = require('./routes/gallery')
const travel = require('./routes/travel')
const multer = require('multer')

const app = express()

//Db connect
mongoose.connect(config.database)

mongoose.connection.on('connected', () => {
    console.log('Connected to DB ' + config.database);
})

mongoose.connection.on('error', (err) => {
    console.log('Connected NOT DB ' + err);
})

//Middleware
app.use(cors())
app.use(bodyParser.json())
app.use('/users', users)
app.use('/gallery', router)
app.use('/travel', travel)
app.use(passport.initialize())
app.use(passport.session())
require('./config/passport')(passport)
//save file to gcp
const multerMid = multer({
    storage: multer.memoryStorage(),
    limits: {
      // no larger than 5mb.
      fileSize: 5 * 1024 * 1024,
    },
  });
  app.use(multerMid.single('file'))

//static files
app.use(express.static(path.join(__dirname, 'public')))

//index route
app.get('/', (req, res) => {
    res.send('Invalid Endpoint')
})

app.get('*', (req, res) => {
    res.sendFile(__dirname, '/public/index.html')
})

const PORT = process.env.PORT || 8080

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))